import { connect } from 'mqtt'

const connectClient = (brokerAddress, brokerPort, protocol, brokerUser, brokerPassword, qos) => {
  const client = connect({
    host: brokerAddress,
    port: brokerPort,
    protocol: protocol,
    username: brokerUser,
    password: brokerPassword,
    qos: qos
  })

  client.on('connect', () => {
    console.log('connected')
  })

  return client
}


export default connectClient
