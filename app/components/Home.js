// @flow
import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import routes from '../constants/routes'
import styles from './Home.css'
import { Header, Icon, Form, Input, Select, Divider, Button } from 'semantic-ui-react'

type Props = {}

const mqttProtocols = [
  { key: 'mqtt', text: 'mqtt', value: 'mqtt' },
  { key: 'mqtts', text: 'mqtts', value: 'mqtts' },
  { key: 'tcp', text: 'tcp', value: 'tcp' },
  { key: 'tls', text: 'tls', value: 'tls' },
  { key: 'ws', text: 'ws', value: 'ws' },
  { key: 'wss', text: 'wss', value: 'wss' }
]

const qosType = [
  { key: '0', text: '0', value: '0' },
  { key: '1', text: '1', value: '1' },
  { key: '2', text: '2', value: '2' }
]

const storage = require('electron-storage')

const mqttConfigPath = 'data/mqttConfig.json'

export default class Home extends Component<Props> {
  props: Props

  constructor() {
    super()
    this.state = {
      hostname: '',
      port: '',
      username: '',
      password: '',
      protocol: '',
      qos: '',
      clientId: ''
    }

    storage.isPathExists(mqttConfigPath).then(itDoes => {
      if (itDoes) {
        storage.get(mqttConfigPath).then(data => {
          console.log(data)
          const mqttConfigData = {
            hostname: data.hostname,
            port: data.port,
            username: data.username,
            password: data.password,
            protocol: data.protocol,
            qos: data.qos,
            clientId: data.clientId
          }
          this.setState(mqttConfigData)
        })
      }
    })
  }

  saveConfig = () => {
    const mqttConfig = {
      hostname: this.state.hostname,
      port: this.state.port,
      username: this.state.username,
      password: this.state.password,
      protocol: this.state.protocol,
      qos: this.state.qos,
      clientId: this.state.clientId
    }

    storage.remove(mqttConfigPath).then(err => {
      if (err) {
        console.error(err)
      } else {
        storage.set(mqttConfigPath, mqttConfig, error => {
          if (error) {
            console.error(error)
          } else {
            console.log('Success. Config are saved')
          }
        })
      }
    })
  }

  deleteConfig = () => {
    storage.remove(mqttConfigPath).then(
      err => {
        if (err) {
          console.error(error)
        } else {
          this.setState({
            hostname: '',
            port: '',
            username: '',
            password: '',
            protocol: '',
            qos: '',
            clientId: ''
          })
        }
      }
    )
  }

  handleChange = event => {
    let change = {}
    if (event.target.name != 'protocol' || event.target.name != 'qos') {
      change[event.target.name] = event.target.value
      this.setState(change)
    }
  }

  handleDropDownChangeQos = (event, { value }) => {
    let change = {}
    change['qos'] = value
    this.setState(change)
  }

  handleDropDownChangeProtocol = (event, { value }) => {
    let change = {}
    change['protocol'] = value
    this.setState(change)
  }

  render() {
    return (
      <div className={styles.container} data-tid='container'>
        <Header as='h2'>Broker Server</Header>
        <Divider className={styles.divider} />
        <Form size='large'>
          <Form.Group widths='equal'>
            <Form.Field
              id='form-input-control-broker-hostname'
              name='hostname'
              control={Input}
              label='Broker Hostname'
              placeholder='Broker Hostname'
              onChange={this.handleChange}
              value={this.state.hostname}
            />
            <Form.Field
              id='form-input-control-broker-port'
              name='port'
              control={Input}
              label='Broker Port'
              placeholder='Broker Port'
              onChange={this.handleChange}
              value={this.state.port}
            />
          </Form.Group>
          <Form.Group widths='equal'>
            <Form.Field
              id='form-input-control-broker-username'
              name='username'
              control={Input}
              label='Broker Username'
              placeholder='Broker Username'
              onChange={this.handleChange}
              value={this.state.username}
            />
            <Form.Field
              id='form-input-control-broker-pasword'
              name='password'
              control={Input}
              label='Broker Password'
              placeholder='Broker Password'
              onChange={this.handleChange}
              value={this.state.password}
            />
          </Form.Group>
          <Form.Group widths='equal'>
            <Form.Field
              control={Select}
              options={mqttProtocols}
              name='protcol'
              label={{ children: 'Mqtt Protocol', htmlFor: 'form-select-control-protocol' }}
              placeholder='Mqtt Protocol'
              ref='protocol'
              search
              searchInput={{ id: 'form-select-control-protocol' }}
              value={this.state.protocol}
              onChange={this.handleDropDownChangeProtocol}
            />
            <Form.Field
              control={Select}
              options={qosType}
              name='qos'
              label={{ children: 'QoS', htmlFor: 'form-select-control-qos' }}
              placeholder='QoS'
              ref='qos'
              search
              searchInput={{ id: 'form-select-control-qos' }}
              value={this.state.qos}
              onChange={this.handleDropDownChangeQos}
            />
          </Form.Group>
          <Form.Group widths='equal'>
            <Form.Field
              id='form-input-control-client-id'
              name='clientId'
              control={Input}
              label='Client Id'
              placeholder='Client Id'
              onChange={this.handleChange}
              value={this.state.clientId}
            />
          </Form.Group>
          <Button.Group className={styles.buttons}>
            <Button positive onClick={this.saveConfig}>Save</Button>
            <Button.Or />
            <Button negative onClick={this.deleteConfig}>Delete</Button>            
          </Button.Group>
        </Form>
      </div>
    )
  }
}
