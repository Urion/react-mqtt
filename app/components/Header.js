import React, { Component } from 'react'
import { Menu, Segment } from 'semantic-ui-react'
import { Link } from 'react-router-dom'
import routes from '../constants/routes'

class Header extends Component {
  state = { activeItem: 'broker' }

  handleItemClick = (e, { name }) => this.setState({ activeItem: name })

  render() {
    const { activeItem } = this.state

    return (
      <Segment inverted>
        <Menu inverted secondary>
          <Menu.Item
            name='broker'
            active={activeItem === 'broker'}
            onClick={this.handleItemClick}
            as={Link}
            to={routes.HOME}
          />
          <Menu.Item
            name='subscribe'
            active={activeItem === 'subscribe'}
            onClick={this.handleItemClick}
            as={Link}
            to={routes.SUBSCRIBE}
          />
        </Menu>
      </Segment>
    )
  }
}

export default Header
