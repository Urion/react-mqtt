// @flow
import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import styles from './Subscribe.css'
import routes from '../constants/routes'
import { Header, Divider, Form, Input, Button } from 'semantic-ui-react'
import connectClient from '../services/MqttClient'

type Props = { }

const storage = require('electron-storage')

const mqttConfigPath = 'data/mqttConfig.json'

let client

export default class Subscribe extends Component<Props> {
  props: Props

  constructor () {
    super()

    this.state = {
      topics: [],
      mqttConfig: {

      },
      messages: []
    }

    storage.isPathExists(mqttConfigPath).then(itDoes => {
      if (itDoes) {
        storage.get(mqttConfigPath).then(data => {
          console.log(data)
          const mqttConfigData = {
            mqttConfig: {
              hostname: data.hostname,
              port: data.port,
              username: data.username,
              password: data.password,
              protocol: data.protocol,
              qos: data.qos,
              clientId: data.clientId
            }
          }
          this.setState(mqttConfigData)
        })
      }
    })
  }

  connectToBroker = () => {
    client = connectClient(
      this.state.mqttConfig.hostname,
      this.state.mqttConfig.port,
      this.state.mqttConfig.protocol,
      this.state.mqttConfig.username,
      this.state.mqttConfig.password,
      this.state.mqttConfig.qos
    )
  }

  disconnectFromBroker = () => {
    if (client) {
      if (client.connected) {
        client.end()
        console.log('Client disconnected')
      }
    }
  }

  handleChange = (event)  => {
    let change = {}
    change = {
      topics: [
        event.target.value
      ]
    }

    this.setState(change)
    console.log(this.state.topics)
  }

  subscribeTopic = () => {
    console.log(`Subscribed to ${this.state.topics}`)
    if (client) {
      console.log(`Subscribed to ${this.state.topics}`)
      if (client.connected) {
        this.state.topics.forEach(topic => {
          client.subscribe(topic)
          .on('message', function(topic, message) {
            console.log(message)
            const mess = {topic, message}
            const messages = [...this.state.messages, mess]
            this.setState(messages)
            console.log(this.state.messages)
          })
        })
      }
    }
  }

  render() {
    return (
      <div className={styles.container} data-tid='container'>
        <span className={styles.title}>
          <Header as='h2'>Subscribe</Header>
        </span>
        <span className={styles.btnGroup}>
          <Button.Group>
            <Button positive onClick={this.connectToBroker}>Connect</Button>
            <Button.Or />
            <Button negative onClick={this.disconnectFromBroker}>Disconnect</Button>
          </Button.Group>
        </span>
        <Divider className={styles.divider} />
        <Form onSubmit={this.subscribeTopic}>
          <Form.Group widths='equal'>
            <Form.Field
              id='form-input-topic'
              control={Input}
              label='Topic'
              placeholder='Topic'
              onChange={this.handleChange}
              value={this.state.topics[0]}
            />
          </Form.Group>
          <Form.Button content='Subscribe'/>
        </Form>
        <Divider className={styles.divider} />
        <div className={styles.messageContainer} data-tid='container'>

        </div>
      </div>
    )
  }
}
