// @flow
import React, { Component } from 'react'
import Subscribe from '../components/Subscribe'

type Props = {}

export default class SubscribePage extends Component<Props> {
  props: Props

  render() {
    return <Subscribe />
  }
}
