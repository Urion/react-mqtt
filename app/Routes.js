import React from 'react'
import { Switch, Route } from 'react-router'
import routes from './constants/routes'
import App from './containers/App'
import HomePage from './containers/HomePage'
import SubscribePage from './containers/SubscribePage'
import Header from './components/Header'

export default () => (
  <App>
    <Header />
    <Switch>
      <Route path={routes.SUBSCRIBE} component={SubscribePage} />
      <Route path={routes.HOME} component={HomePage} />
    </Switch>
  </App>
)
